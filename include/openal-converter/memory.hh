#pragma once
#include <memory>

struct AVFormatContext;
struct AVCodecContext;
struct SwrContext;
struct AVCodec;
struct AVPacket;
struct AVFrame;

namespace oal_converter
{
    class format_context_deleter_type
    {
    public:
        void operator()(AVFormatContext* format_context) const;
    };
    using format_context_pointer_type = std::unique_ptr<AVFormatContext, format_context_deleter_type>;

    class codec_context_deleter_type
    {
    public:
        void operator()(AVCodecContext* codec_context) const;
    };
    using codec_context_pointer_type = std::unique_ptr<AVCodecContext, codec_context_deleter_type>;

    class packet_deleter_type
    {
    public:
        void operator()(AVPacket* packet) const;
    };
    using packet_pointer_type = std::unique_ptr<AVPacket, packet_deleter_type>;

    class frame_deleter_type
    {
    public:
        void operator()(AVFrame* frame) const;
    };
    using frame_pointer_type = std::unique_ptr<AVFrame, frame_deleter_type>;

    class resampler_context_deleter_type
    {
    public:
        void operator()(SwrContext* resample_context) const;
    };
    using resampler_context_pointer_type = std::unique_ptr<SwrContext, resampler_context_deleter_type>;
}
