#pragma once
#include <vector>
#include <cinttypes>
#include "memory.hh"

namespace oal_converter
{
    using frames_type = std::vector<frame_pointer_type>;

    enum class format_type: uint8_t
    {
        u8,
        s16,
    };

    enum class channel_layout_type: uint8_t
    {
        mono,
        stereo,
    };
}
