#pragma once
#include <openal-converter/types.hh>

namespace oal_converter
{
    class resampler_type
    {
    public:
        resampler_type(channel_layout_type channel_layout, format_type format);

        bool begin_resample(const frame_pointer_type& frame);
        frame_pointer_type resample(const frame_pointer_type& frame) const;
        void end_resample();

    private:
        void create_resample_context(const frame_pointer_type& frame);

        channel_layout_type _channel_layout;
        format_type _format;
        resampler_context_pointer_type _resampler_context;
    };
}
