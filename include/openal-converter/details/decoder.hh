#pragma once
#include <string>
#include <openal-converter/types.hh>

namespace oal_converter
{
    class decoder_type
    {
    public:
        decoder_type() = default;

        void open(std::string path);
        bool get_frame(const frame_pointer_type& frame);

    private:
        void create_format_context();
        void create_codec_context();
        void create_packet();

        void send_packet();
        bool receive_frame(const frame_pointer_type& frame);

        std::string _path;
        format_context_pointer_type _format_context;
        codec_context_pointer_type _codec_context;
        packet_pointer_type _packet;
        bool _flush = false;
    };
}
