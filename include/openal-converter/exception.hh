#pragma once
#include <exception>
#include <string>
#include <fmt/format.h>

namespace oal_converter
{
    class exception_type: public std::exception
    {
    public:
        template<typename... argument_types>
        exception_type(const fmt::string_view& format, argument_types&&... arguments):
            _message(fmt::format(format, std::forward<argument_types>(arguments)...))
        {
            // Nothing to do yet
        }

    private:
        virtual char const* what() const noexcept override;

        std::string _message;
    };
}
