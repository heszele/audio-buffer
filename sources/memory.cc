#include <openal-converter/memory.hh>

extern "C"
{
    #include <libavformat/avformat.h>
    #include <libavcodec/avcodec.h>
    #include <libswresample/swresample.h>
}

namespace oal_converter
{
    void format_context_deleter_type::operator()(AVFormatContext* format_context) const
    {
        avformat_close_input(&format_context);
    }

    void codec_context_deleter_type::operator()(AVCodecContext* codec_context) const
    {
        avcodec_close(codec_context);
        avcodec_free_context(&codec_context);
    }

    void packet_deleter_type::operator()(AVPacket* packet) const
    {
        av_packet_free(&packet);
    }

    void frame_deleter_type::operator()(AVFrame* frame) const
    {
        av_frame_free(&frame);
    }

    void resampler_context_deleter_type::operator()(SwrContext* resample_context) const
    {
        swr_close(resample_context);
        swr_free(&resample_context);
    }
}
