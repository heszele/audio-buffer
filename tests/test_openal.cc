#ifdef OPENAL_CONVERTER_AUDITORY_TESTS

#include <catch2/catch.hpp>
#include <AL/alc.h>
#include <AL/al.h>
#include <openal-converter/audio_file.hh>
#include <thread>
#include <cmath>

namespace
{
    ALenum get_openal_format(const oal_converter::audio_buffer_type& buffer)
    {
        switch (buffer.get_channel_layout())
        {
        case oal_converter::channel_layout_type::stereo:
            switch(buffer.get_format())
            {
            case oal_converter::format_type::u8:
                return AL_FORMAT_STEREO8;
            case oal_converter::format_type::s16:
                return AL_FORMAT_STEREO16;
            default:
                REQUIRE(false);
            }
            break;
        case oal_converter::channel_layout_type::mono:
            switch(buffer.get_format())
            {
            case oal_converter::format_type::u8:
                return AL_FORMAT_MONO8;
            case oal_converter::format_type::s16:
                return AL_FORMAT_MONO16;
            default:
                REQUIRE(false);
            }
            break;
        default:
            REQUIRE(false);
        }

        // Just to make the compiler happy
        return AL_INVALID_ENUM;
    }
    void play_sound(const std::string& path,
        bool streaming,
        oal_converter::channel_layout_type channel_layout,
        oal_converter::format_type format)
    {
        constexpr long double pi = 3.14159265358979323846264338327950288L;
        oal_converter::audio_buffer_type audio_buffer(channel_layout, format);

        if(streaming)
        {
            audio_buffer = oal_converter::audio_file_type::get_buffer(path, channel_layout, format);
        }
        else
        {
            oal_converter::audio_file_type audio_file(path);
            audio_buffer= audio_file.get_buffer(channel_layout, format);
        }

        // Prepare buffer
        ALuint buffer = 0;
        alGenBuffers(1, &buffer);
        alBufferData(buffer,
                     get_openal_format(audio_buffer),
                     audio_buffer.get_buffer().data(),
                     ALsizei(audio_buffer.get_buffer().size()),
                     audio_buffer.get_frequency());

        // Prepare source
        ALuint source = 0;
        alGenSources(1, &source);
        alSourcei(source, AL_BUFFER, buffer);
        alSourcef(source, AL_GAIN, 1.0);
        alSourcef(source, AL_PITCH, 1.0);
        //alSource3f(source, AL_POSITION, 0, 0 ,0);
        alSource3f(source, AL_DIRECTION, 1, 0, 0);

        // Prepare listener
        alListener3f(AL_POSITION, 0, 0, 0);

        alSourcePlay(source);

        const int sleep = int((1.0f / 60.0f) * 1000);
        ALint status = 0;
        const auto start = std::chrono::system_clock::now();

        alGetSourcei(source, AL_SOURCE_STATE, &status);
        while(status == AL_PLAYING)
        {
            const std::chrono::duration<float> diff = (std::chrono::system_clock::now() - start) * 2 * pi;
            const float x = std::cos(diff.count());
            const float z = std::sin(diff.count());

            alSource3f(source, AL_POSITION, x, 0 ,z);
            std::this_thread::sleep_for(std::chrono::milliseconds(sleep));
            alGetSourcei(source, AL_SOURCE_STATE, &status);
        }

        alDeleteSources(1, &source);
        alDeleteBuffers(1, &buffer);
    }
}

SCENARIO("audio_buffer can be used to play sounds with OpenAL", "[openal]")
{
    GIVEN("An initialized current OpenAL context")
    {
        ALCdevice* device = alcOpenDevice(nullptr);

        REQUIRE(device != nullptr);

        ALCcontext* context =  alcCreateContext(device, nullptr);

        REQUIRE(context != nullptr);

        REQUIRE(ALC_FALSE != alcMakeContextCurrent(context));

        AND_GIVEN("Different audio files")
        {
            THEN("Files can be converted to mono, s16 format and played without streaming")
            {
                const bool streaming = false;
                const oal_converter::channel_layout_type channel_layout = oal_converter::channel_layout_type::mono;
                const oal_converter::format_type format = oal_converter::format_type::s16;

                play_sound("./sounds/test-mono.wav", streaming, channel_layout, format);
                play_sound("./sounds/test-mono.mp3", streaming, channel_layout, format);
                play_sound("./sounds/test-mono.flac", streaming, channel_layout, format);

                play_sound("./sounds/test-stereo.wav", streaming, channel_layout, format);
                play_sound("./sounds/test-stereo.mp3", streaming, channel_layout, format);
                play_sound("./sounds/test-stereo.flac", streaming, channel_layout, format);
            }
            THEN("Files can be converted to mono, s16 format and played with streaming")
            {
                const bool streaming = true;
                const oal_converter::channel_layout_type channel_layout = oal_converter::channel_layout_type::mono;
                const oal_converter::format_type format = oal_converter::format_type::s16;

                play_sound("./sounds/test-mono.wav", streaming, channel_layout, format);
                play_sound("./sounds/test-mono.mp3", streaming, channel_layout, format);
                play_sound("./sounds/test-mono.flac", streaming, channel_layout, format);

                play_sound("./sounds/test-stereo.wav", streaming, channel_layout, format);
                play_sound("./sounds/test-stereo.mp3", streaming, channel_layout, format);
                play_sound("./sounds/test-stereo.flac", streaming, channel_layout, format);
            }
            THEN("Files can be converted to stereo, u8 format and played without streaming")
            {
                const bool streaming = false;
                const oal_converter::channel_layout_type channel_layout = oal_converter::channel_layout_type::stereo;
                const oal_converter::format_type format = oal_converter::format_type::u8;

                play_sound("./sounds/test-mono.wav", streaming, channel_layout, format);
                play_sound("./sounds/test-mono.mp3", streaming, channel_layout, format);
                play_sound("./sounds/test-mono.flac", streaming, channel_layout, format);

                play_sound("./sounds/test-stereo.wav", streaming, channel_layout, format);
                play_sound("./sounds/test-stereo.mp3", streaming, channel_layout, format);
                play_sound("./sounds/test-stereo.flac", streaming, channel_layout, format);
            }
            THEN("Files can be converted to stereo, u8 format and played with streaming")
            {
                const bool streaming = true;
                const oal_converter::channel_layout_type channel_layout = oal_converter::channel_layout_type::stereo;
                const oal_converter::format_type format = oal_converter::format_type::u8;

                play_sound("./sounds/test-mono.wav", streaming, channel_layout, format);
                play_sound("./sounds/test-mono.mp3", streaming, channel_layout, format);
                play_sound("./sounds/test-mono.flac", streaming, channel_layout, format);

                play_sound("./sounds/test-stereo.wav", streaming, channel_layout, format);
                play_sound("./sounds/test-stereo.mp3", streaming, channel_layout, format);
                play_sound("./sounds/test-stereo.flac", streaming, channel_layout, format);
            }
        }

        alcDestroyContext(context);
        alcCloseDevice(device);
    }
}

#endif
