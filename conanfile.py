from conans import ConanFile, CMake, tools
import os

class OpenAlConverterConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    default_options = { "fmt:header_only": True, "spdlog:header_only": True }
    generators = ["cmake"]
    requires = "ffmpeg/4.2.1@bincrafters/stable", "spdlog/1.8.2", "catch2/2.13.4", "openal/1.21.0"

    def requirements(self):
        if self.settings.os == "Linux":
            # On Linux openal requires libalse 1.2.4 while ffmpeg 1.1.9
            # By defining it here, we will force ffmeg to use this one
            self.requires("libalsa/1.2.4")
